package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar datos");
			System.out.println("2. Generar JSON");
			System.out.println("3. Buscar String");
			System.out.println("4. Cargar grafo persistido");
			System.out.println("5. Dar cantidad de componentes conectados");
			System.out.println("6. Graficar");
			System.out.println("7. Eliminar String");
			System.out.println("8. Imprimir el Arreglo");
			System.out.println("9. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			// TODO implementar
		}
}
