package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class GrafoNoDirigido <K,V>{

	
	//Almacernarlo con listas de adyacencia
	
	private int V;
	private ArrayList<Bag<Integer>> adj;
	private ArrayList vertices;
	private ArrayList check;
	private int[] id;
	public GrafoNoDirigido ()
	{
		V=0;
		adj = new ArrayList<>();
	}
	@SuppressWarnings("unchecked")
	public void Graph(int numV){
		this.V = numV;
		adj = new ArrayList<>();
		vertices=new ArrayList<>();
		check = new ArrayList<>();
		for(int v = 0;v < numV; v++) {
			adj.add(new Bag <Integer>());
			getVertices().add( (V) new Vertex<V, K>(v, null));
			check.add(false);
		}
		
	}
	public int id() {
		return id[V()];
	}
	/**
	 * N�mero de v�rtices
	 * @return
	 */
	public int V(){
		return V;
	}
	/**
	 * N�mero de arcos.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public int E(){
		int rta=0;
		Iterator<K> aux;
		for(int i=0;i<V;i++)
		{
			for(int j=0;j<adj.get(i).size();j++)
			{
				aux=(Iterator<K>) adj.get(i).iterator();
				if((Integer)aux.next()>(i)) {
					rta++;
					
				}
			}
		}
		return rta;
	}
	
	public void addEdge(K idVertexIni, K idVertexFin, double cost) {
		boolean existeini=false;
		int id1=0;
		int id2=0;
		boolean existefin=false;
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexIni)
			{
				existeini=true;
				id1=i;
			}
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexFin)
			{
				existefin=true;
				id2=i;
			}
		}
		if(existeini && existefin)
		{
			adj.get(id1).add((int)idVertexFin, cost);
			adj.get(id2).add((int)idVertexIni, cost);
		}
		
	}
	/**
	 * 
	 * @param idVertex
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public V getInfoVertex(K idVertex){
		
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertex)
			{
				return (V) ((Vertex) getVertices().get(i)).getInfo();
			}
			
	}
		return null;
	}
	/**
	 * Modifica la informaci�n del v�rtice idVertex.
	 * @param idVertex
	 * @param infoVertex
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setInfoVertex(K idVertex, V infoVertex){
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertex)
			{
				 ((Vertex) getVertices().get(i)).setInfo(infoVertex);
			}
		}
	}
	/**
	 * Obtiene el costo de un arco, si el arco no existe, retorna -1
	 * @param idVertexIni
	 * @param idVertexFin
	 * @return
	 */
	public double getCostArc(K idVertexIni, K idVertexFin){

		double rta=-1;
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexIni)
			{
				rta= adj.get(i).getCost((int) idVertexFin);
			}
		}
		
		return rta;
	}
	/**
	 * Modifica el costo del arco entre los v�rtices idVertexIni e idVertexFin
	 * @param idVertexIni
	 * @param idVertexFin
	 * @param cost
	 */
	public void setCostArc(K idVertexIni, K idVertexFin, double cost){
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexIni)
			{
			  adj.get(i).setCost((int) idVertexFin,cost);
			}
		}
	}
	/**
	 * 
	 * @param i
	 * @param info
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addVertex(int i, String info){
		if(vertices==null) vertices=new ArrayList<>();
		getVertices().add(new Vertex((int) i,info));
		adj.add(new Bag<>());
		V++;
	}
	
	@SuppressWarnings("unchecked")
	public Iterable <K> adj(int j){
		ArrayList rta = null;
		for(int i=0;i<V ;i++)
		{
			if(((Vertex) getVertices().get(i)).getId()==(int)j)
			{
				Iterator aux =adj.get(i).iterator();
				while(aux.hasNext())
				{
					rta.add(aux.next());
				}
			}
			
		}
		return rta;
	}
	
	@SuppressWarnings("unchecked")
	public void uncheck(){
		check=new ArrayList<>();
		for(int i=0;i<V;i++)
		{
			check.add(false);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void dfs(K s){
		Vertex v = (Vertex) s;
		check.add( v.getId(), true);
		for (K w : adj(v.getId()))
			if ((boolean)check.get((int) getVertices().get((int) s)) == false)
			{
				addEdge((K) w, s, getCostArc(w, s));
				dfs(w);
			}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int cc(){
		int count = 0;
		id = new int[V()];
		for (int s = 0; s < V(); s++) {
			
			Vertex v = (Vertex) getVertices().get(s);
			v.setId(s); v.setInfo(s);
			if ( (boolean)check.get(v.getId()) == false)
			{
				dfs((K)getVertices().get(s));
				id[s] = count;
				count++;
			}
		}

		return count;
	}
	
	@SuppressWarnings("unchecked")
	public Iterable <K> getCC(K idVertex){
		cc();
		ArrayList<K> cs = new ArrayList<>();
		for(int i = 0; i < V(); i++) {
			if(id[i] == id[(int) idVertex])
			{
				cs.add((K) getVertices().get(i));
			}
		}
		return cs;
	}
	public ArrayList getVertices() {
		return vertices;
	}
	public void setVertices(ArrayList vertices) {
		this.vertices = vertices;
	}
	public void addEdge(int idVertexIni, int idVertexFin, double cost) {
		// TODO Auto-generated method stub
		boolean existeini=false;
		int id1=0;
		int id2=0;
		boolean existefin=false;
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexIni)
			{
				existeini=true;
				id1=i;
			}
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexFin)
			{
				existefin=true;
				id2=i;
			}
		}
		if(existeini && existefin)
		{
			adj.get(id1).add((int)idVertexFin, cost);
			adj.get(id2).add((int)idVertexIni, cost);
		}
	}
	public String getInfoVertex(int idVertex) {
		// TODO Auto-generated method stub
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertex)
			{
				return (String) ((Vertex) getVertices().get(i)).getInfo();
			}

		}
		return null;
	}
	
}
