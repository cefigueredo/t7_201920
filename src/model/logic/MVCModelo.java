package model.logic;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JFrame;

import com.google.gson.*;
import com.teamdev.jxmaps.swing.MapView;
import com.teamdev.jxmaps.*;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Vertex;

/**
 * Definicion del modelo del mundo
 * @param <K>
 * @param <V>
 *
 */
public class MVCModelo<K, V> extends MapView{
	/**
	 * Atributos del modelo del mundo
	 */
	private GrafoNoDirigido<K, V> graph;
	private int arc;
	private Map map;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	@SuppressWarnings("unchecked")
	public MVCModelo()
	{
		graph=new GrafoNoDirigido();
	}
	
	
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return (Integer) null;
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(String dato)
	{	
		
	}
	
	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public String buscar(String dato)
	{
		return null;
	}
	
	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public String eliminar(String dato)
	{
		return null;
	}
	public void cargarDatos() throws Exception
	{
		System.out.println("Cargando datos...");
		long begin=System.currentTimeMillis();
		String read=null;
		BufferedReader reader=new BufferedReader( new FileReader(new File("./data/bogota_vertices.txt")));
		reader.readLine();
		String[] data = new String[4];
		int cont=0;
		while((read=reader.readLine())!=null)
		{
			data=read.split(";");
			String info=data[1]+","+data[2]+","+data[3];
			graph.addVertex(Integer.parseInt(data[0]),info ); 
			cont++;			
			
			/***********************************/ if(cont == 10000)break;
		}
		System.out.println("Se cargaron "+cont+" vertices.");
		reader=new BufferedReader(new FileReader("./data/bogota_arcos.txt"));
		cont=0;
		while((read=reader.readLine())!=null)
		{
			data = read.split(" ");
			for(int i=1;i<data.length;i++)
			{
				if(graph.getInfoVertex(Integer.parseInt( data[0])) != null &&  graph.getInfoVertex(Integer.parseInt( data[i]))!=null)
				{
					double cost=Haversine.distance(Double.parseDouble(( (String) graph.getInfoVertex(Integer.parseInt( data[0]))).split(",")[1])
							, Double.parseDouble(((String) graph.getInfoVertex(Integer.parseInt( data[0]))).split(",")[0])
							, Double.parseDouble(((String) graph.getInfoVertex(Integer.parseInt( data[i]))).split(",")[1])
							,Double.parseDouble( ((String) graph.getInfoVertex(Integer.parseInt(data[i]))).split(",")[0])); 
					graph.addEdge(Integer.parseInt(data[0]), Integer.parseInt(data[i]),cost ); 
					cont++;
					if(cont%14000==0) System.out.println("Cargando, por favor espere.");
				}
			}
			
		}
		arc = cont;
		
		System.out.println("Se cargaron "+arc+" arcos.");
		System.out.println("Tiempo requerido: "+(System.currentTimeMillis()-begin)+" ms");
	}
	public void generarJSON() throws Exception
	{
		PrintWriter pw= new PrintWriter(new FileWriter("./data/persistencia.json"));
		JsonArray array= new JsonArray();
		for(int i=0; i<graph.V();i++)
		{
			JsonObject obj=new JsonObject();
			obj.addProperty("InternalID", ((Vertex) graph.getVertices().get(i)).getId());
			obj.addProperty("ArrayID",i);
			obj.addProperty("Longitud",graph.getInfoVertex(((Vertex) graph.getVertices().get(i)).getId()).split(",")[1]);
			obj.addProperty("Latitud",graph.getInfoVertex(((Vertex) graph.getVertices().get(i)).getId()).split(",")[0]);
			String aux="";
			for(int j=0;j<((ArrayList) graph.adj(((Vertex) graph.getVertices().get(i)).getId())).size();j++)
			{
				aux+=((ArrayList) graph.adj(((Vertex) graph.getVertices().get(i)).getId())).get(j)+" ";
			}
			obj.addProperty("adj", aux);
			array.add(obj);
		}
		String json=array.toString();
		pw.println(json);
	}

	@SuppressWarnings("deprecation")
	public void cargarGrafoPersistido() throws Exception {
		
		int nvert = 0; int narc = 0;
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject)parser.parse(new FileReader("./data/persistencia.json"));
		JsonArray tamVert = (JsonArray)obj.get("Longitud").getAsJsonArray();
		nvert = tamVert.size();
		JsonArray tamAdj = (JsonArray)obj.get("adj").getAsJsonArray();
		narc = tamAdj.size(); narc = arc;
		
		System.out.println("N�mero de vertices: "+nvert+"\nN�mero de arcos: "+narc);	
	}
	
	public void darCantidadCC() {
		int totalCC = 0;
		graph.cc();
		totalCC = graph.id();
		System.out.println("Cantidad total de componentes conectados: "+totalCC);
	}
	
	private void grafica(String nName) throws Exception {
		double longMax =  -74.062707;
		double longMin =  -74.094723;
		double longMedia = (longMax + longMin)/2;
		double latMax = 4.621360;
		double latMin = 4.597714;
		double latMedia = (latMax + latMin)/2;

		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject)parser.parse(new FileReader("./data/persistencia.json"));
		JsonArray tamLng = (JsonArray)obj.get("Longitud").getAsJsonArray();
		JsonArray tamLat = (JsonArray)obj.get("Latitud").getAsJsonArray();
		JFrame frame = new JFrame();
		setOnMapReadyHandler(new MapReadyHandler() {
			
			@Override
			public void onMapReady(MapStatus status) {
				if(status == MapStatus.MAP_STATUS_OK) {					
					map = getMap();
					
					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					mapOptions.setMapTypeControlOptions(controlOptions);
					
					map.setOptions(mapOptions);
					map.setCenter(new LatLng(latMedia, longMedia));
					map.setZoom(11.9);			
					for(int i = 0; i < tamLat.size(); i++) {
						Circle circle = new Circle(map);
						LatLng ll = new LatLng(tamLat.get(i).getAsDouble(), tamLng.get(i).getAsDouble());
						circle.setCenter(ll);
						circle.setRadius(2);
						
						CircleOptions co = new CircleOptions();
						co.setFillColor("aff0000");
						co.setFillOpacity(0.35);
						circle.setOptions(co);
					}				
				}			
			}
		});
		
		
		frame.add(this, BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setVisible(true);
		
		
		System.out.println("Graficado");
	}
	public void graficar() throws Exception {
		grafica("Ventana");
	}
}
